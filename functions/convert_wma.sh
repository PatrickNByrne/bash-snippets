convert_wma () {
  # If we get an argument, set it as the search dir
  # If not, use the current directory
  search_directory="${1:-$(pwd)}"

  # Set output directory
  output_directory="$(pwd)"

  # Find the WMA files
  wma_files=$(find "${search_directory}" -type f -iname "*.wma")

  # Need to change IFS or files with filenames containing spaces will not
  # be handled correctly by for loop
  OIFS=$IFS
  IFS=$'\n'
  
  # Loop through the files and convert to mp3
  for wma_file in ${wma_files}; do
   # Set output file name
   mp3_file="$(basename $wma_file | sed 's/.wma/.mp3/')"
   ffmpeg -i "${wma_file}" -q:a 0 "${output_directory}/${mp3_file}"
   #uncomment rm below to delete original wma's
   #rm "${wma_file}"
  done
  
  # Restore IFS
  IFS=$OIFS
}
