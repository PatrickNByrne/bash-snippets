# Test if a directory exists and create it if it doesn't
check_dir()
{
  if [[ ! -d "$1" ]]; then
    mkdir -p "$1";
  fi
}
