# Replace program/function output with a progress bar
## Example: prog_bar "yum update"
prog_bar(){
  # Call the command, redirect output, and background the process
  ${1} > /dev/null 2>&1 &
  # Capture the PID of the last run command
  progresspid=$!

  # Output a character until the command finishes
  while [[ -d /proc/$progresspid ]]; do
    printf '█'
    sleep 0.1s
  done
}
