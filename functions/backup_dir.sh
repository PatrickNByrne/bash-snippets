# These functions are used to backup a directory and cleanup old backups

# Accepts two arguments: 
# 1) Path to the backup target dir 2) Path to store the backup tar.bz2 files 
backup_dir()
{
  if [[ ! -d "$1" ]]; then
    echo "Error: $1 is not a valid directory"
    exit 1
  elif [[ ! -d "$2" ]]; then
    echo "Error: $2 is not a valid directory"
    exit 1
  else
    tar -cjf "${2}/backup-$(date +%m-%d-%Y).tar.bz2" "$1" 
  fi
}

## Accepts two arguments: 
# 1) Path to backup folder 2) Number of backups to keep
clean_backup_dir()
{
	LASTDATE=$(date --date "${curr} -${2} day" +%m-%d-%Y)
	if [[ -f "$1/backup-${LASTDATE}.tar.bz2" ]]
	then
		rm "$1/backup-${LASTDATE}.tar.bz2"
	fi
}
