# Colorize status message output
## Example: status_msg info "Checking download speed"
status_msg(){
  if [[ "${1,,}" == "info" ]]; then
    printf "\e[1;36m%s\e[0m\n" "[INFO] $2"
  elif [[ "${1,,}" == "warn" ]]; then
    printf "\e[1;33m%s\e[0m\n" "[WARN] $2"
  elif [[ "${1,,}" == "error" ]]; then
    printf "\e[1;31m%s\e[0m\n" "[ERROR] $2"
  elif [[ "${1,,}" == "success" ]]; then
    printf "\e[1;32m%s\e[0m\n" "[SUCCESS] $2"
  fi
}

# Another version that can handle custom tags
status_msg(){
  # Define colors
  red='\e[1;31m%s\e[0m\n'
  green='\e[1;32m%s\e[0m\n'
  cyan='\e[1;36m%s\e[0m\n'
  yellow='\e[1;33m%s\e[0m\n'

  # Output status message
  case ${1,,} in
    note)
      printf "$cyan" "[NOTE] $2"
    ;;
    warn)
      printf "$yellow" "[WARN] $2"
    ;;
    error)
      printf "$red" "[ERROR] $2"
    ;;
    ok)
      printf "$green" "[OK] $2"
    ;;
    *)
      if [[ -z "$2" ]]; then
        printf "[INFO] $1\n"
      else
        printf "[${1}] $2\n"
      fi
    ;;
  esac
}

# Additional colors
black='\e[1;30m%s\e[0m\n'
red='\e[1;31m%s\e[0m\n'
green='\e[1;32m%s\e[0m\n'
yellow='\e[1;33m%s\e[0m\n'
blue='\e[1;34m%s\e[0m\n'
magenta='\e[1;35m%s\e[0m\n'
cyan='\e[1;36m%s\e[0m\n'
light_gray='\e[1;37m%s\e[0m\n'
dark_gray='\e[1;90m%s\e[0m\n'
light_red='\e[1;91m%s\e[0m\n'
light_green='\e[1;92m%s\e[0m\n'
light_yellow='\e[1;93m%s\e[0m\n'
light_blue='\e[1;94m%s\e[0m\n'
light_magenta='\e[1;95m%s\e[0m\n'
light_cyan='\e[1;96m%s\e[0m\n'
white='\e[1;97m%s\e[0m\n'

# Background colors
bg_default='\e[1;49m%s\e[0m\n'
bg_black='\e[1;40m%s\e[0m\n'
bg_red='\e[1;41m%s\e[0m\n'
bg_green='\e[1;42m%s\e[0m\n'
bg_yellow='\e[1;43m%s\e[0m\n'
bg_blue='\e[1;44m%s\e[0m\n'
bg_magenta='\e[1;45m%s\e[0m\n'
bg_cyan='\e[1;46m%s\e[0m\n'
bg_light_gray='\e[1;47m%s\e[0m\n'
bg_dark_gray='\e[1;100m%s\e[0m\n'
bg_light_red='\e[1;101m%s\e[0m\n'
bg_light_green='\e[1;102m%s\e[0m\n'
bg_light_yellow='\e[1;103m%s\e[0m\n'
bg_light_blue='\e[1;104m%s\e[0m\n'
bg_light_magenta='\e[1;105m%s\e[0m\n'
bg_light_cyan='\e[1;106m%s\e[0m\n'
bg_white='\e[1;107m%s\e[0m\n'
