#!/bin/bash
# ------------------------------------------------------------------
# Copyright:  Patrick Byrne
# License:      GNU GPLv3
# Author:       Patrick Byrne
# Title:      
# Description:
#         
# ------------------------------------------------------------------
#    Todo:
# ------------------------------------------------------------------

# --- Variables ----------------------------------------------------

version=0.1.0

# --- Functions ----------------------------------------------------

version()	
{
  printf "Version: %s\n" $version
}

usage() 
{
    cat <<"STOP"
    Usage: xxx.sh [OPTIONS]

    OPTIONS

      -h            Print this message
      -v            Print Version 

      --help same as -h
      --version same as -v 

STOP
}

# --- Options processing -------------------------------------------

while [[ $# -gt 0 ]]; do
  param=$1
  value=$2
  case $param in
    -h | --help | help)
      usage
      exit
      ;;
    -v | --version | version)
      version
      exit
      ;;
    *)
      echo "Error: unknown parameter \"$param\""
      usage
      exit 1
      ;;
  esac
  shift
done

# --- Body ---------------------------------------------------------



